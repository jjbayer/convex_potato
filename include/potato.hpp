#pragma once

#include "polygon.hpp"
#include "visualizer.hpp"

#include <vector>
#include <algorithm>
#include <memory>

#include <boost/optional.hpp>


//bool contains(const cv::Point3f & point) const;
template<typename T>
class PotatoFinder
{

public:

    using Vec3 = cv::Vec<T, 3>;
//    using Vec3Container = std::vector<Vec3>;

    using Poly = Polygon<T>;

    PotatoFinder(const Poly & input,
                 std::shared_ptr<Visualizer<T> > visualizer = nullptr,
                 T epsilon = std::numeric_limits<T>::epsilon())
        : polygon_(input)
        , visualizer_(visualizer ? visualizer : std::make_shared<DummyVisualizer<T> >(input))
        , epsilon_(epsilon)
    {

    }


    void findAllPotatoes(std::vector<Polygon<T> > & output)
    {
        output.clear();

        for(size_t i = 0; i < polygon_.size(); i++) {
            output.push_back(findPotato(i));
        }
    }

    Polygon<T> findMaxAreaPotato()
    {
        std::vector<Polygon<T> > potatoes;
        findAllPotatoes(potatoes);
        if( potatoes.empty() ) {
            // No potatoes found, return empty polygon
            return Polygon<T>();
        }

        auto it = std::max_element(potatoes.begin(), potatoes.end(), [](const Poly & a, const Poly & b) {
            return a.area() < b.area();
        });

        assert(it != potatoes.end());

        return std::move(*it);
    }

    Polygon<T>
    findPotato(size_t start)
    {
        using Poly = Polygon<T>;

        Polygon<T> output;

        const auto n = polygon_.size();

        if( n < 3 ) throw InvalidPolygon("Potato for 2 points or less makes no sense");

        auto point = [start, n, this](size_t i) {
            return polygon_.point((start + i) % n);
        };

        const auto p0 = point(0);
        const auto p1 = point(1);
        const auto firstLine = line(p0, p1);

        output.push(p0);
        visualizer_->markAdded(output, start % n);
        output.push(p1);
        visualizer_->markAdded(output, (start + 1) % n);

        std::unique_ptr<Vec3> badLine = nullptr;

        for(size_t i = 2; i < polygon_.size(); i++) {

            const auto & p = point(i);

            /*  ___
             *     \
             *   |  \
             *   \__/
             *
             *
             */

            if( badLine ) {

                if( badLine->dot(p) < +epsilon_ ) {
                    // Back in the game
                    badLine = nullptr;
                } else {
                    // Right of bad line. Ignore.
                    visualizer_->markIgnored(output, (start+i)%n);
                    continue;
                }
            }

            if( output.embraces(p, epsilon_) ) {
                while( output.size() > 2 && output.closingLine().dot(p) < -epsilon_ ) {
                    visualizer_->markViolation(output, (start+i)%n);
                    output.pop();
                }
                visualizer_->markAdded(output, (start+i)%n);
                output.push(p);
            } else {
                if(output.size() > 2) {
                    const auto lastPoint = output.point(output.size() - 1);
                    if( (firstLine.dot(p) > epsilon_) && (output.closingLine().dot(p) > epsilon_) ) {
                        badLine = std::make_unique<Vec3>(line(lastPoint, p));
                    }
                }

                visualizer_->markIgnored(output, (start+i)%n);
            }
        }

        visualizer_->markFinished(output);

        return std::move(output);
    }

private:

    const Poly & polygon_;
    std::shared_ptr<Visualizer<T> > visualizer_;
    T epsilon_;

};

