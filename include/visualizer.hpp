#pragma once

#include "polygon.hpp"

#include <opencv2/highgui/highgui.hpp>


template<typename T>
class Visualizer
{
public:

    using Vec3 = cv::Vec<T, 3>;
    using Poly = Polygon<T>;
    using Index = size_t;

    Visualizer(const Poly & polygon): polygon(polygon) {}

    /// Mark a point that is added to the convex potato
    virtual void markAdded(const Poly & potato, const Index & index) = 0;

    /// Mark a point that is ignored
    virtual void markIgnored(const Poly & potato, const Index & index) = 0;

    /// Mark a point that is excluded
    virtual void markViolation(const Poly & potato, const Index & index) = 0;

    virtual void markFinished(const Poly & potato) = 0;


    virtual ~Visualizer() {}

    const Poly & polygon;

};


template<typename T>
class DummyVisualizer: public Visualizer<T>
{

public:

    using Poly = Polygon<T>;
    using Index = size_t;
    using Vec3 = typename Poly::Vec3;

    DummyVisualizer(const Poly & polygon): Visualizer<T>(polygon) {}

    void markAdded(const Poly & potato, const Index &index) override {}
    void markIgnored(const Poly & potato, const Index &index) override {}
    void markViolation(const Poly & potato, const Index &index) override {}

    void markFinished(const Poly & potato) override {}
};


template<typename T>
class HighGUIVisualizer: public Visualizer<T> // TODO: for double
{

public:

    using Parent = Visualizer<T>;
    using Poly = Polygon<T>;
    using Index = size_t;
    using Vec3 = typename Poly::Vec3;


    HighGUIVisualizer(const Poly &polygon)
        : Parent(polygon)
        , image_(cv::Mat::zeros(500, 500, CV_8UC3)) // TODO dynamic
    {
        cv::namedWindow(windowName());

        for(const auto & v : polygon.points()) {
            points_.push_back(cv::Point2f {v(0) / v(2), v(1) / v(2)});
        }

        cv::polylines(image_, points_, true, cv::Scalar(255, 255, 255), 2);
        cv::imshow(windowName(), image_);
        cv::waitKey(1000); // TODO
    }

    void markAdded(const Poly & potato, const size_t & index) override
    {
        draw(potato, index, {0, 255, 0});
    }

    void markIgnored(const Poly & potato,const size_t & index) override
    {
        draw(potato, index, {128, 128, 128});
    }

    void markViolation(const Poly & potato,const size_t &index) override
    {
        draw(potato, index, {0, 0, 255});
    }

    void markFinished(const Poly & potato) override
    {
        draw(potato, 0, {255, 0, 0}, true);
    }

    ~HighGUIVisualizer()
    {
        cv::destroyWindow(windowName());
    }

private:
    const char * windowName() const { return "convex_potato"; }

    void draw(const Poly & potato, size_t index, const cv::Scalar &color, bool closed = false)
    {
        image_ = cv::Mat::zeros(500, 500, CV_8UC3);

            decltype(points_) potatoPoints;

            for(const auto & v : potato.points()) {
                potatoPoints.push_back(cv::Point2f {v(0) / v(2), v(1) / v(2)});
            }

            cv::polylines(image_, points_, true, cv::Scalar(255, 255, 255), 2);

            cv::polylines(image_, potatoPoints, closed, cv::Scalar(0, 255, 0), 2);

            if(potatoPoints.size() > 1) {
                const auto & firstPoint = potatoPoints[0];
                const auto & lastPoint = *potatoPoints.rbegin();
                cv::line(image_, lastPoint, firstPoint, cv::Scalar(0, 255, 0), 1);

                if(potatoPoints.size() > 2) {
                    auto embrace0 = potatoPoints[0] + 1000* (potatoPoints[0] - potatoPoints[1]);
                    cv::line(image_, embrace0, firstPoint, cv::Scalar(255, 255, 0), 1);
                    auto embrace1 = *potatoPoints.rbegin() + 1000* (*potatoPoints.rbegin() - *(potatoPoints.rbegin() + 1));
                    cv::line(image_, embrace1, lastPoint, cv::Scalar(255, 255, 0), 1);

                }
            }

            cv::circle(image_, points_.at(index), 5, color, 2);
            cv::imshow(windowName(), image_);

            cv::waitKey(0); // TODO
        }

    cv::Mat image_;
    std::vector<cv::Point> points_;
};


