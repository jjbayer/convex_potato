#pragma once

#include <opencv2/core/core.hpp>

#include <limits>
#include <stdexcept>

template<typename T>
cv::Vec<T, 3> vec2to3(const cv::Vec<T,2> & v)
{
    return {v(0), v(1), 1.f};
}

template<typename T>
cv::Vec<T, 2> vec3to2(const cv::Vec<T,3> & v)
{
    return {v(0) / v(2), v(1) / v(2)};
}

template<typename T>
cv::Point vec2toPoint(const cv::Vec<T,2> & v)
{
    return {
        static_cast<int>(v(0)),
        static_cast<int>(v(1))
    };
}

template<typename T>
cv::Point vec3toPoint(const cv::Vec<T,3> & v)
{
    return vec2toPoint(vec3to2(v));
}

template<typename T>
cv::Vec<T,2> pointToVec2(const cv::Point & p)
{
    return {
        p.x, p.y
    };
}

template<typename T>
cv::Vec<T,2> point2fToVec2(const cv::Point2f & p)
{
    return {
        p.x, p.y
    };
}

template<typename T>
cv::Vec<T,3> pointToVec3(const cv::Point & p)
{
    return vec2to3(pointToVec2<T>(p));
}

template<typename T>
cv::Vec<T,3> point2fToVec3(const cv::Point2f & p)
{
    return vec2to3(point2fToVec2<T>(p));
}



template<typename T>
cv::Vec<T,3> line(const cv::Vec<T,3> & p0, const cv::Vec<T,3> & p1)
{
    using Vec2 = typename cv::Vec<T,2>;
    using Vec3 = typename cv::Vec<T,3>;

    Vec3 l = p0.cross(p1);
    auto length = norm(Vec2(l(0), l(1)));/*
    if( length < std::numeric_limits<typename TT::Scalar>::epsilon() ) {
        throw std::runtime_error("Line too short");
    }*/

    l /= length; // normalize s.t. geom. length == 1

    return l;
}

