#pragma once

#include "geometry.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <stdexcept>


struct InvalidPolygon: public std::runtime_error
{
    InvalidPolygon(const std::string & what)
        :std::runtime_error(what)
    {}
};


///
///
/// Vec3Container must offer size_type
///
template<typename T>
class Polygon
{

public:

    using Vec2 = cv::Vec<T, 2>;
    using Vec2Container = std::vector<Vec2>;

    using Vec3 = cv::Vec<T, 3>;
    using Vec3Container = std::vector<Vec3>;

    explicit Polygon() {

    }

    explicit Polygon(const Vec3Container & points)
        : points_(points), lines_({})
    {

    }

    explicit Polygon(const std::vector<cv::Point> & points)
    {
        for(const auto & point : points) {
            points_.push_back(pointToVec3<T>(point));
        }
    }

    explicit Polygon(const std::vector<cv::Point2f> & points)
    {
        for(const auto & point : points) {
            points_.push_back(point2fToVec3<T>(point));
        }
    }

    Polygon(const Polygon &) = delete;

    Polygon(const Polygon && other)
        :points_(std::move(other.points_))
        , lines_(std::move(other.lines_))
    {

    }

    size_t size() const { return points_.size(); }

    void push(const Vec3 & p)
    {
        if( size() ) {
            auto l = line(point(size() - 1), p);
            lines_.push_back(l);
        }
        points_.push_back(p);
    }

    void pop()
    {
        points_.pop_back();
        if( ! lines_.empty() ) {
            lines_.pop_back();
        }
    }

    const Vec3 & point(size_t index) const {
        // TODO: use [] in release
        return points_.at(index);
    }

    T area() const
    {
        return cv::contourArea(polyline());
    }

    Vec3 closingLine() const
    {
        if( size() < 3 ) {
            throw InvalidPolygon("Polygon with size < 3 has no closing line");
        }

        return line(point(size() - 1), point(0));
    }

    ///
    bool embraces(const Vec3 & newPoint, T epsilon = std::numeric_limits<T>::epsilon()) const
    {
        for(const auto & line : lines_) {
            if( line.dot(newPoint) > epsilon ) {

                return false;
            }
        }

        return true;
    }

    const Vec3Container & points() const
    {
        return points_;
    }

    /// Convert it back to OpenCV polygon
    std::vector<cv::Point> polyline() const
    {
        std::vector<cv::Point> ret;
        for(const auto & point : points()) {
            ret.push_back(vec3toPoint(point));
        }

        return ret;
    }

    friend std::ostream& operator<< (std::ostream& stream, const Polygon<T> & polygon)
    {

        stream << "{";
        for(const auto & point : polygon.points()) {
            auto p = vec3to2(point);
            stream << "{" << p(0) << "," << p(1) << "},";
        }
        stream << "}";

        return stream;
    }


private:

    Vec3Container points_;
    Vec3Container lines_;

};

