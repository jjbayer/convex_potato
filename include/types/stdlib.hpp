#pragma once

#include <vector>

// Forward declaration
template<typename T>
struct ContainerTraits;


// Partial template specialization
template<>
template<typename T>
struct ContainerTraits<std::vector<T> >
{
    using Container = std::vector<T>;
    using Vec3Type = T;
    using SizeType = typename Container::size_type;

    static inline void pop(Container & v) { v.pop_back(); }
    static inline void push(Container & v, const Vec3Type & value) { v.push_back(value); }

};
